#!/bin/sh

external_monitor=$(xrandr --query | grep 'HDMI')
if [[ $external_monitor = *connected* ]]; then
  xrandr --output LVDS-1 --right-of HDMI-1 --mode 1366x768 --pos 1920x0 --rotate normal --output VGA-1 --off --output HDMI-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal
fi
