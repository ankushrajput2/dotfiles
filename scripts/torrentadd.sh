#!/bin/sh

#script to add torrent to transmission.

pgrep -x transmission-da >/dev/null || (transmission-daemon && notify-send "Starting transmission.." && sleep 3)

transmission-remote -a "$@" && notify-send "Torrent added."
