#!/bin/sh

#script to notify when torrent is done downloading.

notify-send "$TR_TORRENT_NAME has finished downloading."
